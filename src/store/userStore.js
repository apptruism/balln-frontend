import { registerUser, loginUser, fetchUser } from '@/services/usersService'

export default {
  namespaced: true,
  state: {
    jwt: localStorage.getItem('Jwt'),
    userId: undefined,
    displayName: undefined,
    email: undefined
  },
  mutations: {
    setUser (state, userParams) {
      state.jwt = userParams.jwt
      state.userId = userParams.id
      state.displayName = userParams.displayName
      state.email = userParams.email
    }
  },
  actions: {
    fetchUser ({ commit }) {
      return fetchUser()
        .then(response => {
          commit('setUser', {
            jwt: response.attributes.auth_token,
            id: response.id,
            displayName: response.attributes.display_name,
            email: response.attributes.email
          })
          return response
        })
    },
    registerUser ({ commit }, userParams) {
      return registerUser({
        email: userParams.email,
        password: userParams.password
      })
        .then(response => {
          console.log(response)
          commit('setUser', {
            jwt: response.attributes.auth_token,
            id: response.id,
            displayName: response.attributes.display_name,
            email: response.attributes.email
          })
          localStorage.setItem('Jwt', response.attributes.auth_token)
          return response
        })
    },
    loginUser ({ commit }, userParams) {
      return loginUser({
        email: userParams.email,
        password: userParams.password
      })
        .then(response => {
          commit('setUser', {
            jwt: response.jwt,
            id: response.user.id,
            displayName: response.user.attributes.display_name,
            email: response.user.attributes.email
          })
          localStorage.setItem('Jwt', response.jwt)
          return response
        })
    }
  }
}
