import { fetchDiscover } from '@/services/eventsService'

export default {
  namespaced: true,
  state: {
    events: []
  },
  mutations: {
    setEvents (state, events) {
      state.events = events
    }
  },
  actions: {
    fetchEvents ({ commit }) {
      return fetchDiscover()
        .then(response => {
          commit('setEvents', response)
        })
    }
  }
}
