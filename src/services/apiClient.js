const BASE_URL = process.env.VUE_APP_API_BASE_URL

export function apiFetch (endpoint, options) {
  return fetch(`${BASE_URL}/${endpoint}`, {
    ...options,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('Jwt')
    }
  })
}
