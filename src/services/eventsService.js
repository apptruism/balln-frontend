import { apiFetch } from './apiClient'

export function fetchDiscover () {
  return apiFetch('games/discover')
    .then(response => response.json())
}
