import { apiFetch } from './apiClient'

export function registerUser (userParams) {
  return apiFetch('users', {
    method: 'post',
    body: JSON.stringify({
      user: {
        email: userParams.email,
        password: userParams.password
      }
    })
  })
    .then(response => response.json())
    .then(response => response.data)
}

export function loginUser (userParams) {
  return apiFetch('login', {
    method: 'post',
    body: JSON.stringify({
      auth: {
        email: userParams.email,
        password: userParams.password
      }
    })
  })
    .then(response => response.json())
}

export function fetchUser () {
  return apiFetch('users/me')
    .then(response => response.json())
    .then(response => response.data)
}

export function updateUser (id) {
  return apiFetch(``)
}
