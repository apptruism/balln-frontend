import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

import Dashboard from '@/views/Dashboard'
import Discover from '@/views/Discover'
import PastGames from '@/views/PastGames'
import ProfileAccount from '@/views/Profile/Account'
import ProfileYourGames from '@/views/Profile/YourGames'
import SignIn from '@/views/Sign/SignIn'
import Register from '@/views/Sign/Register'
import EventCreate from '@/views/Event/Create'
import EventShow from '@/views/Event/Show'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      meta: { requiresAuth: true }
    },
    {
      path: '/discover',
      name: 'discover',
      component: Discover,
      meta: { requiresAuth: true }
    },
    {
      path: '/past-games',
      name: 'past_games',
      component: PastGames,
      meta: { requiresAuth: true }
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfileAccount,
      meta: { requiresAuth: true }
    },
    {
      path: '/profile/games',
      name: 'profile_games',
      component: ProfileYourGames,
      meta: { requiresAuth: true }
    },
    {
      path: '/event/create',
      name: 'event_create',
      component: EventCreate,
      meta: { requiresAuth: true }
    },
    {
      path: '/event/:id',
      name: 'event_show',
      component: EventShow,
      meta: { requiresAuth: true }
    },
    {
      path: '/signin',
      name: 'sign_in',
      component: SignIn
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.user.jwt) {
      next({
        path: '/signin',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

router.afterEach(() => {
  window.scrollTo(0, 0)
})

export default router
