import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

import { Navigation } from '@/components'

Vue.config.productionTip = false

Vue.component('Navigation', Navigation)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
