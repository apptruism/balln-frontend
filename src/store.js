import Vue from 'vue'
import Vuex from 'vuex'

import UserStore from '@/store/userStore'
import DiscoverViewStore from '@/store/discoverViewStore'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user: UserStore,
    discoverView: DiscoverViewStore
  }
})
